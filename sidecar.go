package main

import (
        "fmt"
        "net/http"
)

func main() {
        http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
                fmt.Fprint(w, "Hello from the sidecar!\n")
        })

        fmt.Println("Sidecar server listening on :8081")
        http.ListenAndServe(":8081", nil)
}
