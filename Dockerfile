FROM golang:alpine
WORKDIR /app
COPY sidecar.go go.mod ./
RUN go build -o sidecar .
CMD ["./sidecar"]
